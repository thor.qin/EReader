package com.github.thorqin.reader.activities.setting

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SpinnerAdapter
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.github.thorqin.reader.App
import com.github.thorqin.reader.R
import com.github.thorqin.reader.databinding.SettingsActivityBinding
import com.github.thorqin.reader.services.TTSService

class SettingsActivity : AppCompatActivity() {

	private val app: App
		get() {
			return application as App
		}

	private lateinit var binding: SettingsActivityBinding

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		binding = SettingsActivityBinding.inflate(layoutInflater)
		setContentView(binding.root)

		setSupportActionBar(binding.toolbar)
		supportActionBar?.title = getString(R.string.settings)
		supportActionBar?.setDisplayHomeAsUpEnabled(true)

		// Click to flip
		binding.clickToFlipSwitch.isChecked = app.config.clickToFlip
		binding.clickToFlipItem.setOnClickListener {
			binding.clickToFlipSwitch.toggle()
			app.config.clickToFlip = binding.clickToFlipSwitch.isChecked
			app.saveConfig()
		}
		binding.clickToFlipSwitch.setOnClickListener {
			app.config.clickToFlip = binding.clickToFlipSwitch.isChecked
			app.saveConfig()
		}

		// Never lock screen
		binding.screenLockSwitch.isChecked = app.config.neverLock
		binding.screenLockItem.setOnClickListener {
			binding.screenLockSwitch.toggle()
			app.config.neverLock = binding.screenLockSwitch.isChecked
			app.saveConfig()
		}
		binding.screenLockSwitch.setOnClickListener {
			app.config.neverLock = binding.screenLockSwitch.isChecked
			app.saveConfig()
		}

		// Volume Button to Flip
		binding.volumeToFlipSwitch.isChecked = app.config.volumeFlip
		binding.volumeToFlip.setOnClickListener {
			binding.volumeToFlipSwitch.toggle()
			app.config.volumeFlip = binding.volumeToFlipSwitch.isChecked
			app.saveConfig()
		}
		binding.volumeToFlipSwitch.setOnClickListener {
			app.config.volumeFlip = binding.volumeToFlipSwitch.isChecked
			app.saveConfig()
		}

		binding.useMsEngine.isChecked = app.config.ttsEngine == "ms"
		binding.engineSetting.visibility = if (binding.useMsEngine.isChecked) View.VISIBLE else View.GONE
		binding.msEngine.setOnClickListener {
			binding.useMsEngine.toggle()
		}
		binding.useMsEngine.setOnCheckedChangeListener { buttonView, isChecked ->
			app.config.ttsEngine = if (binding.useMsEngine.isChecked) "ms" else "phone"
			binding.engineSetting.visibility = if (binding.useMsEngine.isChecked) View.VISIBLE else View.GONE
			app.saveConfig()
		}


		val array = TTSService.voiceMap.keys.toTypedArray()
		val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, array)
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
		binding.useAnnouncer.adapter = adapter
		binding.useAnnouncer.setSelection( array.indexOf(app.config.msEngineVoice).let { if (it < 0) 0 else it } )
		binding.useAnnouncer.onItemSelectedListener = object:AdapterView.OnItemSelectedListener {
			override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
				(view as TextView).setTextColor(app.getColor(R.color.colorWhite))
				view.textAlignment = TextView.TEXT_ALIGNMENT_VIEW_END
				app.config.msEngineVoice = array[position]
				app.saveConfig()
			}

			override fun onNothingSelected(parent: AdapterView<*>?) {

			}

		}
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		return when (item.itemId) {
			android.R.id.home -> {
				finish()
				true
			}
			else -> super.onOptionsItemSelected(item)
		}
	}

}
